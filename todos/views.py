from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def show_TodoList(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todos/list.html", context)


def show_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "show_detail": todo_list,
    }
    return render(request, "todos/detail.html", context)


def create_TodoList(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("show_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_TodoList(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("show_detail", id=id)
    else:
        form = TodoListForm(instance=todo_list)
    context = {
        "todo_list": todo_list,
        "form": form,
    }
    return render(request, "todos/update.html", context)


def delete_TodoList(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("show_TodoList")
    return render(request, "todos/delete.html")


def create_TodoItem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect("show_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def update_TodoItem(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("show_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    context = {
        "todo_item": todo_item,
        "form": form,
    }
    return render(request, "todos/update_item.html", context)
