from todos.views import (
    show_TodoList,
    show_detail,
    create_TodoList,
    update_TodoItem,
    update_TodoList,
    delete_TodoList,
    create_TodoItem,
)
from django.urls import path


urlpatterns = [
    path("", show_TodoList, name="show_TodoList"),
    path("<int:id>/", show_detail, name="show_detail"),
    path("create/", create_TodoList, name="create_TodoList"),
    path("<int:id>/update/", update_TodoList, name="update_TodoList"),
    path("<int:id>/delete/", delete_TodoList, name="delete_TodoList"),
    path("items/create/", create_TodoItem, name="create_TodoItem"),
    path("items/<int:id>/update/", update_TodoItem, name="update_TodoItem"),
]
